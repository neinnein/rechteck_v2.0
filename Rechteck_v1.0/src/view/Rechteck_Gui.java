package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logic.FarbigesRechteck;
import logic.Punkt;
import logic.Rechteck_Logic;

public class Rechteck_Gui extends JFrame {

	private JPanel contentPane;
	private JTextField tfdX;
	private JTextField tfdY;
	private JTextField tfdRechteck_id;
	private JTextField tfdFarbe;
	private JTextField tfdLaenge;
	private JTextField tfdHoehe;
	private Rechteck_Logic rlogic = new Rechteck_Logic();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rechteck_Gui frame = new Rechteck_Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Rechteck_Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlEingabe = new JPanel();
		contentPane.add(pnlEingabe, BorderLayout.CENTER);
		pnlEingabe.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblRechteckid = new JLabel("Rechteck_id");
		pnlEingabe.add(lblRechteckid);
		
		JLabel lblNewLabel = new JLabel("");
		pnlEingabe.add(lblNewLabel);
		
		tfdRechteck_id = new JTextField();
		pnlEingabe.add(tfdRechteck_id);
		tfdRechteck_id.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("");
		pnlEingabe.add(lblNewLabel_1);
		
		JLabel lblX = new JLabel("X");
		pnlEingabe.add(lblX);
		
		JLabel lblY = new JLabel("Y");
		pnlEingabe.add(lblY);
		
		tfdX = new JTextField();
		pnlEingabe.add(tfdX);
		tfdX.setColumns(10);
		
		tfdY = new JTextField();
		pnlEingabe.add(tfdY);
		tfdY.setColumns(10);
		
		JLabel lblLnge = new JLabel("Länge");
		pnlEingabe.add(lblLnge);
		
		JLabel lblHoehe = new JLabel("Höhe");
		pnlEingabe.add(lblHoehe);
		
		tfdLaenge = new JTextField();
		pnlEingabe.add(tfdLaenge);
		tfdLaenge.setColumns(10);
		
		tfdHoehe = new JTextField();
		pnlEingabe.add(tfdHoehe);
		tfdHoehe.setColumns(10);
		
		JLabel lblFarbe = new JLabel("Farbe");
		pnlEingabe.add(lblFarbe);
		
		JLabel label = new JLabel("");
		pnlEingabe.add(label);
		
		tfdFarbe = new JTextField();
		pnlEingabe.add(tfdFarbe);
		tfdFarbe.setColumns(10);
		
		JButton btnFarbeAuswhlen = new JButton("Farbe auswählen");
		pnlEingabe.add(btnFarbeAuswhlen);
		
		JPanel pnlButtons = new JPanel();
		contentPane.add(pnlButtons, BorderLayout.SOUTH);
		
		JButton btnSpeichern = new JButton("speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonSpeichernClicked();
			}
		});
		pnlButtons.add(btnSpeichern);
		
		JButton btnAnzeigen = new JButton("anzeigen");
		pnlButtons.add(btnAnzeigen);
		
		JButton btnndern = new JButton("ändern");
		pnlButtons.add(btnndern);
	}

	protected void buttonSpeichernClicked() {
		int x = Integer.parseInt(tfdX.getText());
		int y = Integer.parseInt(tfdY.getText());
		int laenge = Integer.parseInt(tfdLaenge.getText());
		int hoehe = Integer.parseInt(tfdHoehe.getText());
		int farbwert = Integer.parseInt(tfdFarbe.getText());
		Color farbe = new Color(farbwert);
		FarbigesRechteck fr = new FarbigesRechteck(new Punkt(x,y), laenge, hoehe, farbe);
		rlogic.rechteck_Eintragen(fr);
	}

}
