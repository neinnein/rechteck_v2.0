CREATE DATABASE rechtecke;
USE rechtecke;
CREATE TABLE t_rechtecke (
  P_Rechteck_id INT PRIMARY KEY,
  x INT DEFAULT NULL,
  y INT DEFAULT NULL,
  laenge INT DEFAULT NULL,
  hoehe INT DEFAULT NULL,
  farbe INT DEFAULT NULL
);